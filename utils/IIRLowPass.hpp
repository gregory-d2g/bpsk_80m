//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#pragma once

class IIRLowPass
{
	private:
		const float K;
		const float O;

		float y_reg;

		static constexpr float time_constant_to_alpha(const float time_constant, const float Ts)
		{
			return Ts / (Ts + time_constant);
		}

	public:
		IIRLowPass(const float RC, const float Ts) :
			K{IIRLowPass::time_constant_to_alpha(RC, Ts)}, O{1.f - K}, y_reg{0.f}
		{}

		float step(const float input)
		{
			y_reg = y_reg * O + input * K;

			return y_reg;
		}

		float output() const
		{
			return y_reg;
		}
};