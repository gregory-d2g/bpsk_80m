//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#include <stdint.h>

class PID
{
	private: 
		const float kp;
		const float ki;

		const float OUTPUT_MIN;
		const float OUTPUT_MAX;

		float integrator;
		float last_error;
		float current_output;

	public:
		PID(const float kp, const float ki, const float OUTPUT_MIN, const float OUTPUT_MAX) :
			kp{kp}, ki{ki}, OUTPUT_MIN{OUTPUT_MIN}, OUTPUT_MAX{OUTPUT_MAX}, last_error{0.f}, integrator{0.f}
		{}

		float step(const float error)
		{
			float output = kp * error + ki * integrator;

			if (output > OUTPUT_MIN && output < OUTPUT_MAX) 
			{
				integrator += (error + last_error) / 2.f;
			}
			else if (output > OUTPUT_MAX) 	
			{
				output      = OUTPUT_MAX;
				if (error < 0.f) integrator += (error + last_error) / 2.f;
			}
			else if (output < OUTPUT_MIN) 
			{
				output      = OUTPUT_MIN;
				if (error > 0.f) integrator += (error + last_error) / 2.f;
			}

			last_error     = error;
			current_output = output;

			return output;
		}

		float output() const
		{
			return current_output;
		}

};