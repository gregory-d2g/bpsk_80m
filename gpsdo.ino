//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti


#include "leds.hpp"
#include "phase_detector.hpp"
#include "pps_sync.hpp"
#include "pwm_dac.hpp"
#include "utils/IIRLowPass.hpp"
#include "utils/QuasiPeakDetector.hpp"
#include "utils/PID.hpp"

void setup() 
{
	leds::setup();

	//leds::set(0, leds::ON);
	leds::set(0, leds::BLINK_FAST);
	leds::set(1, leds::BLINK_SLOW);

	phase_detector::setup();
	pps_sync::setup();
	pwm_dac::setup();


	Serial.begin(19200);

	//pwm_dac::set_word(0);
	//pwm_dac::set_output(0.5f); while(1);
}

PID vco_controller{.02f, 0.0004f, -.5f, +.5f};
QuasiPeakDetector phase_QP_MSE{0.2f, 5.f, 1.f}; 

const float phase_setpoint = 127.f;

uint8_t measures[16];
int8_t  measure_index = -1;


uint32_t a = 0;
uint8_t  b = 0;

void loop() 
{
	const uint32_t current_timestamp = millis();

	leds::process(current_timestamp);
	pps_sync::process(current_timestamp);		

/*
	if (millis() - a > 2000)
	{
		a = millis();

		if (b) 	(pwm_dac::set_word((0xFFFF >> 1) + 1), b = 0);
		else	(pwm_dac::set_word((0xFFFF >> 1) + 2), b = 1);
	}
*/

	if (pps_sync::new_edge())
	{
		const float phase_error = pps_sync::phase_error;

		pwm_dac::set_output(.5f -+ vco_controller.step(phase_error));

		Serial.print("phase error: ");
		Serial.println(pps_sync::phase_error);
		
		Serial.print("output:      ");
		Serial.println(vco_controller.output(), 5);

		Serial.print("phase MSE:   ");
		Serial.println(sqrt(phase_QP_MSE.step(phase_error * phase_error)), 5);
	}
}
