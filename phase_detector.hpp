//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#pragma once

#include <stdint.h>
	
namespace phase_detector
{
	void setup()
	{
		DDRC &= (1 << 0);  // bit 3
		DDRC &= (1 << 1);  // bit 2
		DDRC &= (1 << 2);  // bit 1
		DDRC &= (1 << 3);  // bit 0

		DDRD &= (1 << 2);  // bit 7
		DDRD &= (1 << 3);  // bit 6
		DDRD &= (1 << 4);  // bit 5
		DDRD &= (1 << 5);  // bit 4

		DDRC |=   (1 << 4); // register reset [active high]
		PORTC &= ~(1 << 4);
	}

	inline void __up_reset()
	{
		PORTC |= (1 << 4);
	}

	inline void __down_reset()
	{
		PORTC &= ~(1 << 4);
	}

	uint8_t __fetch_register()
	{
		uint8_t _reg = 0;

		_reg |= (PINC & (1 << 0)) << 3;
		_reg |= (PINC & (1 << 1)) << 1;
		_reg |= (PINC & (1 << 2)) >> 1;
		_reg |= (PINC & (1 << 3)) >> 3;

		_reg |= (PIND & (1 << 2)) << 5;
		_reg |= (PIND & (1 << 3)) << 3;
		_reg |= (PIND & (1 << 4)) << 1;
		_reg |= (PIND & (1 << 5)) >> 1;

		return _reg;
	}
}