//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#include <stdint.h>

namespace leds
{
	const uint32_t BLINK_ON  = 80;
	const uint32_t BLINK_OFF = 600;

	const uint8_t N_LEDS       = 2;
	const uint8_t PINS[N_LEDS] = {12, 11};

	void setup()
	{
		for (uint8_t i = 0; i < N_LEDS; i++)
		{
			pinMode(PINS[i], OUTPUT);
		}
	}

	enum : uint8_t
	{
		ON,
		OFF,
		BLINK_SLOW,
		BLINK_FAST
	};

	uint8_t led_state[N_LEDS];
	uint8_t machine_state = 0;
	uint32_t last_update  = 0;

	void set(const uint8_t led, const uint8_t state)
	{
		led_state[led] = state;
	}

	void process(const uint32_t &current_timestamp)
	{
		switch(machine_state)
		{
			case 0: if (current_timestamp - last_update < BLINK_OFF) return; machine_state = 1; break;
			case 1: if (current_timestamp - last_update < BLINK_ON)  return; machine_state = 2; break;
			case 2: if (current_timestamp - last_update < BLINK_OFF) return; machine_state = 3; break;
			case 3: if (current_timestamp - last_update < BLINK_ON)  return; machine_state = 0; break;
		}

		last_update = current_timestamp;

		for (uint8_t i = 0; i < N_LEDS; i++)
		{
			switch(led_state[i])
			{
				case ON:         digitalWrite(PINS[i], HIGH);               					  break;
				case OFF:        digitalWrite(PINS[i], LOW);                					  break;
				case BLINK_FAST: digitalWrite(PINS[i], machine_state == 1); 					  break;
				case BLINK_SLOW: digitalWrite(PINS[i], machine_state == 1 || machine_state == 3); break;
			}
		}
	}
}